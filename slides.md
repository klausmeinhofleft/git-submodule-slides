---
author: SnKisuke
title: Git Submodule
lang: en
date: 2019-03-05
---

# Que es git?

## En pocas palabras?

Es un software de control de versiones diseñado por Linus Torvalds

> __y control de versiones?__
>
> Es el método por el cual se puede llevar un registro de los cambios realizados sobre algún "producto" a lo largo del tiempo.

# `git submodule`

## Entonces que es git submodule?

Simplemente un subcomando incluido en git que permite la inserción de repositorios dentro de otro repositorio (El cual pasa a llamarse __"súper proyecto"__ según la documentación).

# cuando deberíamos usarlo?

## Reutilización de Código

Una de las propiedades del submódulo es la posibilidad de conservar su propia historia separada de la del súper proyecto, lo cual permite al/los proyecto usar una versión predefinida a un branch o commit en particular.

##
### Ejemplos:

-  Usar alguna librería externa, programa y/o cualquier otro tipo de assets.
-  Necesitamos mantener una librería que es parte del proyecto pero que también es usada en otros proyectos.

## Cuidar el Tamaño del repositorio

Git suele tener problemas para mantener el tamaño del repositorio lo mas chico posible si su contenido no puede ser comprimido con deltas. El caso mas obvio de esto es la inclusión de archivos del tipo binario (imágenes, videos, audio, etc), lo cual ocasiona que tareas como "`git push`" o "`git pull`" se demore mas de la cuenta.

## Control de Acceso

Ya que los mecanismos de control de git no son lo mejor del mundo, segmentar el código en submódulos permite definir de mejor manera las políticas de lectura escritura que cada desarrollador tiene a cada parte del proyecto.

# Jumping into the command line!

## Agregar un submódulo a nuestro proyecto

```bash
# Creamos un repositorio para probar los comandos =3
git init slides-nekos
# nos posicionamos dentro del directorio
cd slides-nekos
# agregamos un submódulo
git submodule add "git@git.rlab.be:snkisuke/prs-generator.git"
```

##

### También podes elegir el directorio donde sera insertado!

```bash
# git submodule add "<repo_url>" <directorio>
git submodule add "git@git.rlab.be:snkisuke/prs-generator.git" \
lazy-generator
```

## muy lindo pero, que hace esto realmente?

##

### Básicamente se encarga de:

-  Clonar el repositorio en donde se le indico que debía agregarse.
-  Mueve el contenido de `<submódulo>/.git` a "`.git/modules/<submódulo>`".
-  Agregar las entradas pertinentes en `.gitmodules` en el directorio raíz del súper proyecto y también en `.git/config`.

##

#### `ls .git/modules/prs-generator`
```bash
HEAD  branches  config  description  hooks  index  info  \
logs  objects  packed-refs  refs 
```

##

#### `cat .gitmodules`
```bash
...
[submodule "prs-generator"]
    path = prs-generator
    url = git@git.rlab.be:snkisuke/prs-generator.git
...
```

##

#### `cat .git/config`
```bash
...
[submodule "prs-generator"]
    url = git@git.rlab.be:snkisuke/prs-generator.git
    active = true
...
```

## Ohh! nuestro submódulo tiene submódulitis!

Que podríamos hacer en este caso?

##

### Inicializar el sub submódulo

Podemos ingresar en el directorio del submódulo e indicarle que inicialice

```bash
git submodule init
git submodule update
# o en un solo comando
git submodule update --init
```

##

#### O podemos usar directamente desde el directorio raíz del súper proyecto

```bash
# mi favorita porque me ahorra muchos caracteres =3
git submodule update --init --recursive
# si estas muy apurado y no te importa consumir ancho de banda,
# tanto tuyo como del/los dueño/s del/los repo/s
# usa '-j <numero>' para paralelizar la descarga
git submodule update --init --recursive -j 4
```

## Cambios dentro de un submódulo

## 

#### Los subimos de la siguiente forma
```bash
cd <submódulo>
git add <file>
git commit -m "edit some stuff"
```

#### Aqui se puede continuar de 2 formas

## 

#### En el mismo directorio del submódulo.
```bash
git push
```
#### O bien desde el directorio raíz del súper proyecto cuando queramos hacer push a todos los cambios en el súper proyecto
```bash
git push --recurse-submodules=on-demand 
```

## Traernos los últimos cambios del submódulo

Dependiendo de tu métodos de trabajo por ahí el submódulo que usas cambia seguido y necesitas traerte eso para poder seguir trabajando.

Como para casi todos los comando que vimos existen también 2 formas de hacerlo, tal vez hay... _go outside and find it by yourself!_ __=D__

## 

#### Actualizar el súper proyecto y de paso también los submódulos.

```bash
git pull --recurse-submodules
```

#### O también solo actualizar los submódulos
```bash
git submodule update --remote
```

## 

#### O solo un submódulo
```bash
git submodule update --remote <submódulo>
```

#### (Que no eran solos 2 formas?)

#### (Si, la segunda y la tercera son la misma, solo que se puede atomizar =P)

## Recursividad!

A veces tenemos que hacer algo muy aburrido varias veces en cada uno de los submódulos que tenemos, Linus también lo padeció por eso nos lego esto =)

## 

#### git submodule foreach

(Para los vagos como yo esto es __ORO__)

Básicamente permite ejecutar arbitrariamente comandos en cada directorio de submódulo

```bash
git submodule foreach 'ls -lh'
...
Entering 'prs-generator'
total 52K
-rw-r--r-- 1 ki7sun3 ki7sun3  34K mar  5 14:25 LICENSE
...
```

## 

#### También soporta recursividad para los submódulo de tu submódulo =D
```bash
git submodule foreach --recursive 'ls -lh'
...
Entering 'prs-generator'
total 52K
-rw-r--r-- 1 ki7sun3 ki7sun3  34K mar  5 14:25 LICENSE
...
Entering 'prs-generator/reveal.js'
total 116K
-rw-r--r--  1 ki7sun3 ki7sun3  512 mar  5 14:25 bower.json
...
```

## Todo tiene un final!

### `git submodule deinit`!
_Otro titulo: A veces querés probar algo nuevo._

## 

#### Con esta guiá de 3 o 4 simples pasos vas a poder lograrlo!

```bash
# En el directorio raíz del súper proyecto
# Paso 0 (opcional)
mv <submódulo> <submódulo_tmp>
# Paso 1
git submodule deinit -f -- <submódulo>
# Paso 2
rm -rf .git/modules/<submodule>
# paso 3 (podes usar cualquiera de sus 2 formas)
git rm -f <submódulo> 
# paso 3 (segunda forma)
# por si querés mantener los archivos en tu repo
git rm --cached <submódulo>
mv <submódulo_tmp> <submódulo>
```

# Cosas que no te voy a contar!

## Pero te las resumo así nomas para que las busques por tu cuenta!

## Lista de comandos

 -  __absorbgitdirs:__ Mueve el directorio .git del submódulo dentro de .git del súper proyecto.
 -  __status:__ Muestra el estado de un submódulo.
 -  __summary:__ Muestra la cantidad de cambios entre un commit dado y el estado actual del directorio del submódulo.
 -  __sync:__ Sincroniza las configuraciones de un submódulo.

# The End

## Referencias:

 -  [Documentación Oficial](https://git-scm.com/docs/git-submodule)
 -  [Mas Documentación Oficial](https://git-scm.com/docs/gitsubmodules)
 -  [Tutorial Oficial](https://git.wiki.kernel.org/index.php/GitSubmoduleTutorial) (esta medio viejuno pero te puede servir)
 -  [Gente Grosa de Stackoverflow](https://stackoverflow.com/questions/1260748/how-do-i-remove-a-submodule)
 -  [Mas Gente Grosa de Stackoverflow](https://stackoverflow.com/questions/5542910/how-do-i-commit-changes-in-a-git-submodule)
 -  [Otro tutorial](https://www.vogella.com/tutorials/GitSubmodules/article.html)

## Gracias por escuchar!

Esto fue _git submodules_ para nintendo 64! digo PC!

__Telegram -> [\@ki7sune](@ki7sune)__

__Mis Repos -> [https://git.rlab.be/snkisuke/](https://git.rlab.be/snkisuke/)__

__Mi email -> [snkisuke@rlab.be](mailto:snkisuke@rlab.be)__
